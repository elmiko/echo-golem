var express = require("express");
var bodyParser = require("body-parser");
var winston = require("winston");
const exec = require("child_process").execSync;

var app = express();
app.enable('trust proxy');
app.use(bodyParser.text({type: '*/*'}));

app.post("/", function(req, res) {
  const date = String(exec('date', ['-u'])).trim();
  winston.info(date + ': POST request from ' + req.ip);
  const resp = req.body + '\n';
  res.status(200).send(resp);
});

app.listen(8080, function() {
  winston.info("echo-golem nodejs-express starting on 0.0.0.0:8080");
});
