"""echo golem pytho flask main module

Run this to start the server

    python main.py

It will start listening on `127.0.0.1:8080`
"""
import flask

app = flask.Flask(__name__)

@app.route('/', methods=['POST'])
def root():
    req = flask.request
    res = req.data + b'\n'
    return res

app.run(host='0.0.0.0', port=8080)
