# echo-golem python + flask implementation

This version of `echo-golem` uses the [Python](https://www.python.org) language
with the [Flask](https://flask.pocoo.org) framework.

## Local virtual environment deployment

After activating a new virtual environment:

```
pip install -r requirements.txt
```

Then run the application:

```
python app.py
```

You can test it by opening another terminal window and entering the following:

```
curl http://127.0.0.1:8080 \
     --data "Hello World!" \
     --header "Content-Type:text/plain" \
     --request POST
```

## Local Docker deployment

To create the Docker image, you will need to use the
[source-to-image project](https://github.com/openshift/source-to-image) to
execute the following command:

```
s2i build \
  https://gitlab.com/elmiko/echo-golem \
  centos/python-35-centos7 \
  echo-golem:python-flask \
  --context-dir=python-flask
```

This command will generate the image and place it in your local Docker
registry with the name `echo-golem`. With the image built, you can now run it
through Docker with the following command:

```
docker run --rm -it -p 8080:8080 echo-golem:python-flask
```

Now the echo-golem is listening on your localhost at port 8080, you can test
this with a similar curl command as before.

```
curl http://127.0.0.1:8080 \
     --data "Hello World!" \
     --header "Content-Type:text/plain" \
     --request POST
```


## OpenShift deployment


### Commands from the Quickstart Video

These commands assume you have an active `oc` session from the shell.

#### Create a new project

```
oc new-project echo
```

#### Launch the application using a [source-to-image](https://docs.openshift.org/latest/creating_images/s2i.html) builder

```
oc new-app \
  centos/python-35-centos7~https://gitlab.com/elmiko/echo-golem.git \
  --context-dir=python-flask
```

#### Tail the logs of the builder pod

```
oc logs -f bc/echo-golem
```

#### Expose a route to the application

```
oc expose service echo-golem
```

#### Make a POST to see if it works

```
curl http://`oc get routes/echo-golem --template='{{.spec.host}}'` \
     --data "Hello World!" \
     --header "Content-Type:text/plain" \
     --request POST
```

#### Examine the application logs

```
oc logs dc/echo-golem
```
