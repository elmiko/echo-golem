# echo-golem rust + hyper implementation

This version of `echo-golem` uses the [rust](https://rust-lang.org) language
with the [hyper](https://hyper.rs/).


## Local Podman deployment

To create the Podman image, you will need to use the
[source-to-image project](https://github.com/openshift/source-to-image) to
create a Dockerfile and associated artifacts, then build the image using
Podman.

1. Make a directory to hold the Dockerfile and artifacts.
   ```
   BUILD_DIR=`mktemp -d`
   ```
1. Create the Dockerfile and artifacts.
   ```
   s2i build https://gitlab.com/elmiko/echo-golem.git \
       quay.io/elmiko/rust-centos8 \
       --context-dir=rust-hyper \
       --as-dockerfile=$BUILD_DIR/Dockerfile
   ```
1. Build the image.
   ```
   podman build -t echo-golem:rust-hyper \
       -f $BUILD_DIR/Dockerfile $BUILD_DIR
   ```
1. Run the image.
   ```
   podman run --rm -it -p 8080:8080 echo-golem:rust-hyper
   ```
1. Validate the service.
   ```
   curl http://127.0.0.1:8080 \
        --data "Hello World!" \
        --header "Content-Type:text/plain" \
        --request POST
   ```

## Local Docker deployment

To create the Docker image, you will need to use the
[source-to-image project](https://github.com/openshift/source-to-image) to
execute the following command:

```
s2i build \
  https://gitlab.com/elmiko/echo-golem \
  quay.io/elmiko/rust-centos8 \
  echo-golem:rust-hyper \
  --context-dir=rust-hyper
```

This command will generate the image and place it in your local Docker
registry with the name `echo-golem`. With the image built, you can now run it
through Docker with the following command:

```
docker run --rm -it -p 8080:8080 echo-golem:rust-hyper
```

Now the echo-golem is listening on your localhost at port 8080, you can test
this with a similar curl command as before.

```
curl http://127.0.0.1:8080 \
     --data "Hello World!" \
     --header "Content-Type:text/plain" \
     --request POST
```
