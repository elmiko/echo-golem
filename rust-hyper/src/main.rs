extern crate hyper;
extern crate futures;

use futures::{Stream};
use futures::future::Future;

use hyper::{Chunk, Method, StatusCode};
use hyper::header::ContentLength;
use hyper::server::{Http, Request, Response, Service};

struct EchoGolem;

fn collect_body(chunk: Chunk) -> Response {
    let mut msg = chunk.iter()
        .cloned()
        .collect::<Vec<u8>>();
    msg.push(b'\n');
    let cl = msg.len();
    Response::new()
        .with_header(ContentLength(cl as u64))
        .with_body(msg)
}

impl Service for EchoGolem {
    type Request = Request;
    type Response = Response;
    type Error = hyper::Error;

    type Future = Box<dyn Future<Item=Self::Response, Error=Self::Error>>;

    fn call(&self, req: Request) -> Self::Future {

        match(req.method(), req.path()) {
            (&Method::Post, "/") => {
                Box::new(req.body()
                         .concat2()
                         .map(collect_body)
                 )
            },
            _ => {
                Box::new(futures::future::ok(
                    Response::new().with_status(StatusCode::NotFound)
                ))
            },
        }
    }
}



fn main() {
    println!("echo-golem rust-hyper starting on 0.0.0.0:8080");
    let addr = "0.0.0.0:8080".parse().unwrap();
    let server = Http::new().bind(&addr, || Ok(EchoGolem)).unwrap();
    server.run().unwrap();
}
